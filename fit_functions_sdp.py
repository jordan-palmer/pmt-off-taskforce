#Defining functions that will be used for the PMT fits and event selection

#-------------------------------------------------------------------------------------------------------------------------------
def gaussian(data,rate,x0,sigma):
    import scipy.stats as stat
    return rate*stat.norm.pdf(data,loc=x0,scale=sigma)

#-------------------------------------------------------------------------------------------------------------------------------
def gaussian_r(data,x0,sigma):
    import scipy.stats as stat
    return stat.norm.pdf(data,loc=x0,scale=sigma)

#----------------------------------------------------------------------------------------------------------------------

def pdf(data,params):
    
    h_sphe = gaussian(data, *params[0:3]) 
    h_dphe = gaussian(data, *params[3:6]) 
    #h_overlap = 
 
    sum = h_sphe + h_dphe
    return sum

#----------------------------------------------------------------------------------------------------------------------

def pdf_r(data,params):
    
    h_sphe = gaussian_r(data, *params[1:3]) 
    h_dphe = gaussian_r(data, *params[4:6]) 
 
    sum = params[0]*((1.-params[3])*h_sphe + params[3]*h_dphe)
    return sum

#----------------------------------------------------------------------------------------------------------------------

def pdf_new(data,params):
    
    h_sphe = gaussian_r(data, *params[1:3]) 
    h_dphe = gaussian_r(data, *params[4:6]) 
 
    sum = params[0]*((1.-params[3])*h_sphe + params[3]*h_dphe) + h_dphe*params[6]
    return sum

#----------------------------------------------------------------------------------------------------------------------

def ULikelihood(data,params):
    import numpy as np
    
    sub_sum = params[0]+params[3]
    
    return -2*(np.log(pdf(data,params)).sum()-sub_sum)

#----------------------------------------------------------------------------------------------------------------------

def ULikelihood_r(data,params):
    import numpy as np
    
    sub_sum = params[0]
    
    return -2*(np.log(pdf_r(data,params)).sum()-sub_sum)

#----------------------------------------------------------------------------------------------------------------------

def ULikelihood_new(data,params):
    import numpy as np
    
    sub_sum = params[0]
    
    return -2*(np.log(pdf_new(data,params)).sum()-sub_sum)
#----------------------------------------------------------------------------------------------------------------------

def golden_filter(s1_probability, s2_probability):
    import numpy as np
    """
    Produce boolean array to select golden events with:
        - At least one s1 followed by one S2
        - Other S2s and S1s allowed

    Args:
        s1Probability: JaggedArray in which probability is 1 if pulse is an S1 and 0 otherwise
        s2Probability: JaggedArray in which probability is 1 if pulse is an S2 and 0 otherwise
       
    Returns:
        Boolean 1D array with length corresponding to the number of events in .rq file
    """
    
    total_filter_l=[]
    
    for i,j in zip(s1_probability,s2_probability):
        
        if len(i)==0: #check if event is empty
            total_filter_l.append(False)
        
        else:
            golden_filter = (np.sum(i  == 1) > 0) #at least one S1
            index_first_S1 = np.argmax(i==1) #index of the first S1
            array_after_S1 = j[(index_first_S1+1):] #array of s2_probability after the S1 pulse

            if len(array_after_S1)==0:
                total_filter_l.append(False)
            else:
                golden_filter = golden_filter & (np.sum(array_after_S1 == 1) >0) # at least one S2 after the S1
                golden_filter = golden_filter & (index_first_S1 < (np.argmax(array_after_S1==1)+index_first_S1+1)) 
                total_filter = golden_filter & (np.sum(i[(index_first_S1+1):(np.argmax(array_after_S1==1)+index_first_S1+1)]==1)==0)
                total_filter_l.append(total_filter)

    
    return np.asarray(total_filter_l)

#----------------------------------------------------------------------------------------------------------------------
#def HG_filter(TPCpulseID,TPCHGpulseID):
#    import numpy as np
#    total_filter = []
    
#    for i,j in zip(TPCpulseID,TPCHGpulseID):
#        if len(i) ==0:
#            total_filter.append(false)
            
#        if TPCpulseID[i] == TPCHGpulseID[j]
#----------------------------------------------------------------------------------------------------------------------

def filter_function(rq,total_filter):
    """
    Takes any rq and reduces it to one that includes only golden events.
    
    Args:
        rq to be reduced
        total_filter_l: output of the golden filter function - boolean array with length corresponding to number of events in .rq file before filtering
       
    Returns:
        reduced rq including only golden events
    """
    reduced_array=[]
    
    for i,j in zip(rq,total_filter):
        if (j==True):
            reduced_array.append(i)
    
    return reduced_array

#----------------------------------------------------------------------------------------------------------------------

def drift_time(s1_probability,s2_probability,pulse_start):
    import numpy as np
    
    """
    Takes the s1 and s2 probabilities to locate s1 and s2 in the array and computes drift time.
    """
    
    drift_time=[]
    
    for i,j,k in zip(s1_probability,s2_probability,pulse_start):   
        
        
        index_first_S1 = np.argmax(i==1) #index of the first S1
        S2_prob_array_after_S1 = np.asarray(j[(index_first_S1+1):]) #array of s2_probability after the S1 pulse
        index_S2=np.argmax(S2_prob_array_after_S1==1)+index_first_S1+1 #we add the index of the S2 in the reduced s2 array to the elements that we cut for S1
        k=np.asarray(k)
        
        s1_time=k[index_first_S1]
        s2_time=k[index_S2]
        
        z_drift=s2_time-s1_time  
        
        drift_time.append(z_drift)

    return drift_time

#----------------------------------------------------------------------------------------------------------------------